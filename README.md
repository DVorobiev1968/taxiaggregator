# Краткое техническое задание:
<ol>
<li>Разработка микросервиса Агрегатора такси.</li>
<li>Сервис работает с использованием инфраструктуры интернет, на базе Spring Framework.</li>
<li>Демонстрируется общий интрефейс: от создания объектов, сущностей, до их начальной конфигурации с использованием context</li>
<li>За основу берется описание объектов с рессурса <a href="https://iway.ru/ngapidoc">iWay </a> </li>
<li>Тестовое задание размещается на хостинге (типа Heroku) имеющий общий доступ, с целью демонстрации работы. По готовности я направляю соответствующую ссылку.
В качестве БД используется Postgres.</li>
</ol>

## Backend реализован в виде сервисного приложения, реализующего следующую бизнес-логику:
<ol>
<li>Поиск такси из пункта А в пункт Б.</li>
<li>Предоставление пользователю ценового предложения от эконом до бизнес-класса автомобиля.</li>
<li>Хранение истории поездок.</li>
<li>Бронирование такси, с возможностью его отмены.</li>
<li>Оповещение пользователю о намеченной поездке.</li>
</ol>
<p>Поскольку речь идет о тестовом примере, то в качестве заглушки коннектора агрегатора используем тестовую БД с набором тестовых маршрутов.<br>
Frontend разрабатывать не требуется.
</p>

## Описание БД.
<p>
Таблицы имеют уникальный ключ (ID) для организации реляционной связи (один к одному, однин-ко-многим) отсортированы по ID.
</p>

### Таблица: Users.
<p>информация о зарегистрированном в сервисе водителе такси, 
с указанием характеристик транспортного средства
<table>
    <tr>
        <th>Тип</th>
        <th>Наименование</th>
        <th>Описание</th>
    </tr>
    <tr>
        <td>int</td>
        <td>Id</td>
        <td>идентификатор пользователя (unique+autoincrement)</td>
    </tr>
    <tr>
        <td>String</td>
        <td>First_Name</td>
        <td>Имя</td>
    </tr>
    <tr>
        <td>String</td>
        <td>Last_Name</td>
        <td>Фамилия</td>
    </tr>
    <tr>
        <td>String</td>
        <td>Password</td>
        <td>пароль</td>
    </tr>
    <tr>
        <td>Binary</td>
        <td>Photo</td>
        <td>Фотография</td>
    </tr>
    <tr>
        <td>Object</td>
        <td>Avto</td>
        <td>Связная таблица Car (Relation ID) </td>
    </tr>

</table>

### Таблица: Car.
<p>информация характеристик транспортного средства 
<table>
    <tr>
        <th>Тип</th>
        <th>Наименование</th>
        <th>Описание</th>
    </tr>
    <tr>
        <td>int</td>
        <td>Id</td>
        <td>идентификатор авто (unique+autoincrement)</td>
    </tr>
    <tr>
        <td>int</td>
        <td>User_Id</td>
        <td>идентификатор водителя (CONSTRAINT CONTACT_ID FOREIGN KEY (CONTACT_ID))</td>
    </tr>
    <tr>
        <td>int</td>
        <td>Type</td>
        <td>тип авто (1-стандарт, 2-комфорт, 3 -минивэн.)</td>
    </tr>
    <tr>
        <td>float</td>
        <td>Price</td>
        <td>цена за 1 км. Пути.</td>
    </tr>
    <tr>
        <td>String</td>
        <td>Model</td>
        <td>Марка, модель авто</td>
    </tr>

</table>

## Описание объектов

### Класс Passenger
<p>
Класс описывает клиента который заказывает такси
</p>

<table>
    <tr>
        <th>Тип</th>
        <th>Наименование</th>
        <th>Описание</th>
    </tr>
    <tr>
        <td>String</td>
        <td>name</td>
        <td>Имя пассажира</td>
    </tr>
    <tr>
        <td>String</td>
        <td>phone</td>
        <td>Контактный телефон</td>
    </tr>
    <tr>
        <td>String</td>
        <td>email</td>
        <td>Адрес электронной почты</td>
    </tr>
    <tr>
        <td>int</td>
        <td>companionCount</td>
        <td>Кол-во пассажиров попутчиков</td>
    </tr>
</table>

```
private int id;
private String name;
private String phone;
private String email;
private int companionCount;
```

### Класс Location
<p>
Класс локации определяет местоположение откуда необходимо забрать клиента
</p>

<table>
    <tr>
        <th>Тип</th>
        <th>Наименование</th>
        <th>Описание</th>
    </tr>
    <tr>
        <td>Flight</td>
        <td>flight</td>
        <td>Объект с реквизатами прибытия рейса самолета </td>
    </tr>
    <tr>
        <td>Train</td>
        <td>train</td>
        <td>Объект с реквизатами прибытия поезда </td>
    </tr>
    <tr>
        <td>Address</td>
        <td>address</td>
        <td>Объект с фактическим адресом местонахождения клиента</td>
    </tr>
</table>

```
    private Flight flight;
    private Train train;
    private Address address;

```
###  Класс Trip
<p>
Класс описывает объект поездки. </p>
<table>
    <tr>
        <th>Тип</th>
        <th>Наименование</th>
        <th>Описание</th>
    </tr>
    <tr>
        <td>int</td>
        <td>priceId</td>
        <td>Идентификатор прайса</td>
    </tr>
    <tr>
        <td>Location</td>
        <td>startLocation</td>
        <td>Объект описывающий начало поездки</td>
    </tr>
    <tr>
        <td>Address</td>
        <td>finishLocation</td>
        <td>Объект описывающий окончание поездки</td>
    </tr>
    <tr>
        <td>Passenger</td>
        <td>passenger</td>
        <td>Объект описывающий пассажиров</td>
    </tr>
</table>

```
    private int priceId;
    private Location startLocation;
    private Address finishLocation;
    private Passenger passenger;
```