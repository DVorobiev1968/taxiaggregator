package ru.DVorobiev.taxi.Location;

import lombok.Getter;
import lombok.Setter;

import java.util.Date;

@Getter
@Setter
public class Flight extends AbstractTransport{
    /** номер рейса */
    private int number;
    /** номер терминала */
    private int terminal;
    /** наименование рейса */
    private String name;

    public Flight() {
        super();
        this.number = 200;
        this.terminal = 2;
        this.name = new String("Default name of flight");
    }

    public Flight(int id, int number, int terminal, String name, Date arrivalTime) {
        setId(id);
        this.number = number;
        this.terminal = terminal;
        this.name = name;
        setArrivalTime(arrivalTime);
    }

    public String str(){
        return String.format("id:%3d;number:%5d;terminal:%2d;name:%s;arrivalTime:%s",
                getId(),
                getNumber(),
                getTerminal(),
                getName(),
                getDateString(getArrivalTime()));
    }

}
