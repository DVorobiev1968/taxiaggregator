package ru.DVorobiev.taxi.Location;

import lombok.Getter;
import lombok.Setter;

import java.text.DateFormatSymbols;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

@Getter
@Setter
public class Train extends AbstractTransport {
    /** номер рейса */
    private int number;
    /** наименование рейса */
    private String name;
    /** номер вагона */
    private int carriage;

    public Train() {
        super();
        this.number = 100;
        this.name = new String("Default name of train");
        this.carriage = 1;
    }

    public Train(int id, int number, String name, int carriage, Date arrivalTime) {
        setId(id);
        this.number = number;
        this.name = name;
        this.carriage = carriage;
        setArrivalTime(arrivalTime);
    }
    public String str(){
        return String.format("id:%3d;number:%5d;carriage:%2d;name:%s;arrivalTime:%s",
                getId(),
                getNumber(),
                getCarriage(),
                getName(),
                getDateString(getArrivalTime()));
    }

}
