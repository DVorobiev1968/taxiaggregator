package ru.DVorobiev.taxi.Location;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Address implements IAddress {
    private String city;
    private String street;
    private int house;
    private int entrance;
    private String coordinates;

    public Address() {
        this.city=new String("Пермь");
        this.street=new String("Куйбышева");
        this.house=3;
        this.entrance=1;
        this.coordinates=new String("58.015294, 56.229560");
    }

    public Address(String city, String street, int house, int entrance, String coordinates) {
        this.city = city;
        this.street = street;
        this.house = house;
        this.entrance = entrance;
        this.coordinates = coordinates;
    }


    @Override
    public String str() {
        return String.format("%s;%s;house: %3d;entrance: %d;coordinates:%s",
                this.city,
                this.street,
                this.house,
                this.entrance,
                getCoordinates());
    }
    @Override
    public String getCoordinates(){
        return String.format("%s",this.coordinates);
    }
}
