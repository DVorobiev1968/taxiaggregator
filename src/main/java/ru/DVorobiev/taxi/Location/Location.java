package ru.DVorobiev.taxi.Location;

import lombok.Getter;
import lombok.Setter;
import org.springframework.beans.factory.annotation.Autowired;

@Getter
@Setter
public class Location implements ILocation {
    @Autowired
    private Flight flight;
    @Autowired
    private Train train;
    @Autowired
    private Address address;

    public Location() {
        this.flight = new Flight();
        this.train = new Train();
        this.address = new Address();
    }

    @Autowired
    public Location(Flight flight, Train train, Address address) {
        this.flight = flight;
        this.train = train;
        this.address = address;
    }

    @Override
    public String str() {
        return String.format("fligth:%s\n" +
                        "train:%s\n" +
                        "location:%s",
                flight.str(),
                train.str(),
                address.str());
    }
}
