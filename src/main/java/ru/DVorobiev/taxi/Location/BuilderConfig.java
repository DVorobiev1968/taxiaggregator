package ru.DVorobiev.taxi.Location;

import org.springframework.beans.factory.annotation.Configurable;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Scope;
import ru.DVorobiev.taxi.Passenger.Passenger;
import ru.DVorobiev.taxi.Trip.Trip;

@Configuration
public class BuilderConfig {
    @Bean("addressBean")
    @Scope("prototype")
    public Address target(){
        return new Address();
    }

    @Bean
    @Scope("prototype")
    public Flight flight(){
        return new Flight();
    }

    @Bean
    @Scope("prototype")
    public Train train(){
        return new Train();
    }

    @Bean
    @Scope("prototype")
    public Passenger passenger(){
        return new Passenger();
    }
    @Bean
    @Scope("prototype")
    public Location location(){
        Location location=new Location(flight(), train(), target());
        return location;
    }

    @Bean
    @Scope("prototype")
    public Trip trip(){

        return new Trip();
    }

}
