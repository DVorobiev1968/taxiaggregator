package ru.DVorobiev.taxi.Location;

import lombok.AccessLevel;
import lombok.Getter;
import lombok.Setter;

import java.text.DateFormatSymbols;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

@Getter(AccessLevel.PROTECTED)
@Setter
public abstract class AbstractTransport {

    /** идентификатор записи*/
    private int id;
    /** время прибытия */
    private Date arrivalTime;

    public AbstractTransport() {
        init();
    }
    private void init(){
        this.id=1;
        this.arrivalTime=new Date();
    }
    protected void setId(int id) {
        this.id = id;
    }

    protected void setArrivalTime(Date arrivalTime) {
        this.arrivalTime = arrivalTime;
    }

    /**
     * метод который преобразовывает дату и время с учетом Locale
     *
     * @return String: строка дата время с учетом Locale
     */
    protected String getDateString(Date date) {
        String dateStr, timeStr;
        String pattern;

        SimpleDateFormat simpleDateFormat;
        Locale locale = new Locale("ru", "RU");
        DateFormatSymbols dateFormatSymbols = new DateFormatSymbols(locale);
        dateFormatSymbols.setWeekdays(
                new String[] {
                        "Не используется",
                        "Понедельник",
                        "Вторник",
                        "Среда",
                        "Четверг",
                        "Пятница",
                        "Суббота",
                        "Воскресенье"
                });

        pattern = "dd/MM/yyyy HH:mm:ss.SSS";
        simpleDateFormat = new SimpleDateFormat(pattern, dateFormatSymbols);
        dateStr = simpleDateFormat.format(date);
        return dateStr;
    }

}
