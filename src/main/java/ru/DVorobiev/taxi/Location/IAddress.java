package ru.DVorobiev.taxi.Location;

public interface IAddress {
    String str();
    String getCoordinates();
}
