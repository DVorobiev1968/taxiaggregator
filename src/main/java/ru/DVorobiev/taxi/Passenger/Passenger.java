package ru.DVorobiev.taxi.Passenger;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Passenger implements IPassenger {
    private int id;
    private String name;
    private String phone;
    private String email;
    private int companionCount;

    public Passenger() {
        this.id=1;
        this.name=new String("Иванов Иван Иванович");
        this.phone=new String("+7(342)-111-111-11");
        this.email=new String("example@mail.ru");
        this.companionCount=3;
    }

    public Passenger(int id, String name, String phone, String email, int companionCount) {
        this.id = id;
        this.name = name;
        this.phone = phone;
        this.email = email;
        this.companionCount = companionCount;
    }

    @Override
    public void create() {

    }

    @Override
    public String str() {
        return String.format("id:%d;%s;%s;%s;кол-во:%d",
                this.id,
                this.name,
                this.phone,
                this.email,
                this.companionCount);
    }
}
