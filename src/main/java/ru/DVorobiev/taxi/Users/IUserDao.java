package ru.DVorobiev.taxi.Users;

import java.util.List;

public interface IUserDao {
    List<User>findAll();
    List<User>findAllWithCar();
    String findById(int id);
    String findByName(String LastName, String FirstName);
    void insert(User user);
}
