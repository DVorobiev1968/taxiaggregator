package ru.DVorobiev.taxi.Users;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

@Getter
@Setter
public class Car implements Serializable {
    private int Id;
    private int UserId;
    private int Type;
    private float Price;
    private String Model;

    @Override
    public String toString(){
        return String.format("User id:%3d;Car id:%2d;type:%1d;price:%3.2f;model:%20s",
                UserId, Id, Type,Price,Model);
    }
}
