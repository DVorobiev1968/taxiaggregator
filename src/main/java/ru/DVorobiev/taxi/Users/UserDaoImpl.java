package ru.DVorobiev.taxi.Users;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.BeanCreationException;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.ResultSetExtractor;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Slf4j
public class UserDaoImpl implements IUserDao, InitializingBean {
    private DataSource dataSource;
    private NamedParameterJdbcTemplate namedParameterJdbcTemplate;


    @Override
    public void afterPropertiesSet() throws Exception {
        if (dataSource == null) {
            throw new BeanCreationException("Must set dataSource on UserDao");
        }

        if (namedParameterJdbcTemplate == null) {
            throw new BeanCreationException("Null NamedParameterJdbcTemplate on UserDao");
        }

    }

    public void setDataSource(DataSource dataSource) {
        this.dataSource = dataSource;

        NamedParameterJdbcTemplate namedParameterJdbcTemplate =
                new NamedParameterJdbcTemplate(dataSource);

        this.namedParameterJdbcTemplate = namedParameterJdbcTemplate;
    }

    private void closeConnection(Connection connection) {
        if (connection == null) {
            return;
        }

        try {
            connection.close();
        } catch (SQLException ex) {
            log.error(ex.getMessage());
        }
    }


    @Override
    public List <User> findAll() {
        String sql = "select id, first_name, last_name, password, photo from users";
        return namedParameterJdbcTemplate.query(sql,new UserMapper());
    }

    @Override
    public List<User> findAllWithCar() {
        String sql= "select user.id, user.first_name, user.last_name, user.password, user.photo " +
                ", car.id as car_id, car.type, car.price, car.model "+
                "from users user " +
                "left join cars car on user.id=car.user_id";
        return namedParameterJdbcTemplate.query(sql,new UserWithCarExtractor());
    }

    public List <User> findAllLambda() {
        String sql = "select id, first_name, last_name, password, photo from users";
        return namedParameterJdbcTemplate.query(sql,(resultSet, i)->{
            User user=new User();
            user.setId(resultSet.getInt("id"));
            user.setFirstName(resultSet.getString("first_name"));
            user.setLastName(resultSet.getString("last_name"));
            user.setPassword(resultSet.getString("password"));
            user.setPhoto(resultSet.getString("photo"));
            return user;
                }
            );
    }

    @Override
    public String findById(int id) {
        String sql = "select * from Users where id = :userId";

        Map<String, Object> namedParameters = new HashMap<String, Object>();
        namedParameters.put("userId", id);

        return namedParameterJdbcTemplate.queryForObject(sql,
                namedParameters, String.class);
    }

    @Override
    public String findByName(String LastName, String FirstName) {
        String sql = "select last_name, first_name from Users where last_name = :LastName and first_name =: FirstName";

        Map<String, Object> namedParameters = new HashMap<String, Object>();
        namedParameters.put("LastName", LastName);
        namedParameters.put("FirstName", FirstName);
        return namedParameterJdbcTemplate.queryForObject(sql,
                namedParameters, String.class);
    }

    @Override
    public void  insert(User user) {
        Connection connection=null;
        try {
            String sql = "insert into users (first_name, last_name, password, photo ) " +
                    "values (?,?,?,?)";
            PreparedStatement statement = connection.prepareStatement(sql);
            statement.setString(1,user.getFirstName());
            statement.setString(2, user.getFirstName());
            statement.setString(3, user.getLastName());
            statement.setString(4, user.getPassword());
            statement.execute();
            ResultSet generateID=statement.getGeneratedKeys();
            if (generateID.next()){
                user.setId(generateID.getInt(1));
            }
        } catch (SQLException ex){
            log.error(ex.getMessage());
        }  finally {
            closeConnection(connection);
        }
    }

    private static final class UserMapper implements RowMapper<User>{

        @Override
        public User mapRow(ResultSet resultSet, int i) throws SQLException {
            User user=new User();
            user.setId(resultSet.getInt("id"));
            user.setFirstName(resultSet.getString("first_name"));
            user.setLastName(resultSet.getString("last_name"));
            user.setPassword(resultSet.getString("password"));
            user.setPhoto(resultSet.getString("photo"));
            return user;
        }
    }

    private static final class UserWithCarExtractor implements
            ResultSetExtractor<List<User>> {
        @Override
        public List<User> extractData(ResultSet rs) throws SQLException,
                DataAccessException {
            Map<Integer, User> map = new HashMap<Integer, User>();
            User user = null;

            while (rs.next()) {
                int id = rs.getInt("id");
                user = map.get(id);

                if (user == null) {
                    user = new User();
                    user.setId(id);
                    user.setFirstName(rs.getString("first_name"));
                    user.setLastName(rs.getString("last_name"));
                    user.setPassword(rs.getString("password"));
                    user.setPhoto(rs.getString("photo"));
                    user.setCarList(new ArrayList<Car>());
                    map.put(id, user);
                }

                int carId = rs.getInt("car_id");

                if (carId > 0) {
                    Car car = new Car();
                    car.setId(carId);
                    car.setUserId(id);
                    car.setType(rs.getInt("type"));
                    car.setPrice(rs.getFloat("price"));
                    car.setModel(rs.getString("model"));
                    user.getCarList().add(car);
                }
            }

            return new ArrayList<User>(map.values());
        }
    }
}
