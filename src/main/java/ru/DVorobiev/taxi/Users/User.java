package ru.DVorobiev.taxi.Users;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;
import java.util.List;

@Getter
@Setter
public class User implements Serializable {
    private int Id;
    private String FirstName;
    private String LastName;
    private String Password;
    private String Photo;
    private List<Car> carList;

    @Override
    public String toString() {
        return String.format("User id:%3d;LastName:%20s;FirstName:%20s;Passwd:%10s;Photo:%10s",
                Id, LastName, FirstName, Password, Photo);
    }
}
