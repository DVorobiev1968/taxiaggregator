package ru.DVorobiev.taxi;

import org.springframework.context.support.ClassPathXmlApplicationContext;
import ru.DVorobiev.taxi.Users.Car;
import ru.DVorobiev.taxi.Users.User;
import ru.DVorobiev.taxi.Users.UserDaoImpl;

import java.util.List;

public class ApplicationXML
{
    public static void main( String[] args )
    {
		ClassPathXmlApplicationContext context = 
				new ClassPathXmlApplicationContext("applicationContext.xml");

        UserDaoImpl userDao=context.getBean("userDao",UserDaoImpl.class);
//        List<User> users=userDao.findAll();
//        List<User> users=userDao.findAllLambda();
//        for (User user:users){
//            if (user!=null) {
//                System.out.println(user);
//            }
//        }
        List<User> users=userDao.findAllWithCar();
        for (User user:users){
            if (user.getCarList()!=null) {
                System.out.println(user);
                for (Car car:user.getCarList()) {
                    System.out.println(car);
                }
            }
        }
		context.close();
    }
}
