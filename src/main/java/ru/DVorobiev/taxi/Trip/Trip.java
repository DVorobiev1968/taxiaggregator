package ru.DVorobiev.taxi.Trip;

import lombok.Getter;
import lombok.Setter;
import ru.DVorobiev.taxi.Location.Address;
import ru.DVorobiev.taxi.Location.Location;
import ru.DVorobiev.taxi.Passenger.Passenger;

@Getter
@Setter
public class Trip implements ITrip{
    private int priceId;
    private Location startLocation;
    private Address finishLocation;
    private Passenger passenger;

    public Trip() {
        this.priceId=1;
        this.startLocation=new Location();
        this.finishLocation=new Address();
        this.passenger=new Passenger();
    }

    public Trip(int priceId, Location startLocation, Address finishLocation, Passenger passenger) {
        this.startLocation = startLocation;
        this.finishLocation = finishLocation;
        this.passenger = passenger;
        this.priceId=priceId;
    }

    @Override
    public String str() {
        return String.format("id:%d\n%s\n%s\n%s",
                getPriceId(),
                getStartLocation().str(),
                getFinishLocation().str(),
                getPassenger().str());
    }
}
