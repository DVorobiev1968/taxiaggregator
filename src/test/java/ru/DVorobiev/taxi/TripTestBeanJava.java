package ru.DVorobiev.taxi;

import lombok.extern.slf4j.Slf4j;
import org.junit.Ignore;
import org.junit.Test;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import ru.DVorobiev.taxi.Location.BuilderConfig;
import ru.DVorobiev.taxi.Trip.Trip;

@Ignore
@Slf4j
public class TripTestBeanJava {
    @Test
    public void testTripBean(){
        String errMessage = String.format("Starting test TripTestBeanJava...");
        log.info(errMessage);
        AnnotationConfigApplicationContext context =
                new AnnotationConfigApplicationContext(BuilderConfig.class);
        Trip trip=context.getBean(Trip.class);
        log.info(trip.str());
        context.close();
    }

}
