package ru.DVorobiev.taxi;

import lombok.extern.slf4j.Slf4j;
import org.junit.Ignore;
import org.junit.Test;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import ru.DVorobiev.taxi.Trip.Trip;

@Ignore
@Slf4j
public class TripTestBeanXML {
    @Test
    public void testTripBean(){
        String errMessage = String.format("Starting test testTripBean...");
        log.info(errMessage);
        ClassPathXmlApplicationContext context =
                new ClassPathXmlApplicationContext("applicationContext.xml");
        Trip trip=context.getBean("tripBean",Trip.class);
        log.info(trip.str());
        context.close();
    }

}
