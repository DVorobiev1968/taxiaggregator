package ru.DVorobiev.taxi;

import lombok.extern.slf4j.Slf4j;
import org.junit.Test;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import ru.DVorobiev.taxi.Location.Address;
import ru.DVorobiev.taxi.Location.Flight;
import ru.DVorobiev.taxi.Location.Location;
import ru.DVorobiev.taxi.Location.Train;

@Slf4j
public class LocationTest {
    @Test
    public void testFlight(){
        String errMessage = String.format("Starting test testFlight...");
        log.info(errMessage);
        Flight flight=new Flight();
        log.info(flight.str());
    }
    @Test
    public void testTrain(){
        String errMessage = String.format("Starting test testTrain...");
        log.info(errMessage);
        Train train=new Train();
        log.info(train.str());
        Train train1=new Train(1,1,"train1",1,new java.util.Date(121,9,14,10,10));
        log.info(train1.str());

    }
    @Test
    public void testLocation(){
        String errMessage = String.format("Starting test testLocation...");
        log.info(errMessage);
        Location location=new Location();
        log.info(location.str());
    }
}
