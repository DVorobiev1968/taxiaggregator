package ru.DVorobiev.taxi;

import lombok.extern.slf4j.Slf4j;
import org.junit.Ignore;
import org.junit.Test;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import ru.DVorobiev.taxi.Location.Address;
import ru.DVorobiev.taxi.Location.Location;

@Ignore
@Slf4j
public class LocationTestBeanXML {
    @Test
    public void testLocationBean(){
        String errMessage = String.format("Starting test testLocationBean...");
        log.info(errMessage);
        ClassPathXmlApplicationContext context =
                new ClassPathXmlApplicationContext("applicationContext.xml");

        Location location=context.getBean("locationBean",Location.class);
        String str1=location.str();
        Address address=location.getAddress();
        String str2=address.getCoordinates();
        log.info(str1);
        log.info(str2);
        context.close();
    }

}
