package ru.DVorobiev.taxi;

import lombok.extern.slf4j.Slf4j;
import org.junit.Test;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import ru.DVorobiev.taxi.Location.Address;
import ru.DVorobiev.taxi.Location.BuilderConfig;
import ru.DVorobiev.taxi.Location.Location;

@Slf4j
public class LocationTestBeanJava {
    @Test
    public void testLocationBeanJava(){
        String errMessage = String.format("Starting test testLocationBeanJava...");
        log.info(errMessage);
        AnnotationConfigApplicationContext context =
                new AnnotationConfigApplicationContext(BuilderConfig.class);

        Location location=context.getBean(Location.class);
        String str1=location.str();
        Address address=location.getAddress();
        String str2=address.getCoordinates();
        log.info(str1);
        log.info(str2);
        context.close();
    }

}
